/**
 * Initialize the Game and starts it.
 */
let game = new Game();
let director = new Director();

function init() {
    game.init();
}

game.ready = window.setInterval(function(){checkReadyState()}, 1000);