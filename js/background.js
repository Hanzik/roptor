/**
 * Creates the Background object which will become a child of
 * the Drawable object. The background is drawn on the "background"
 * canvas and creates the illusion of moving by panning the image.
 */
function Background(image) {

    this.image = image;
    this.defaultSpeed = 0.5;
    this.warpSpeed = 30;
    this.speed = 0.5;
    this.repeat = true;
    this.enteringWarp = false;
    this.leavingWarp = false;

    this.acceleration = 1.02;

    this.init = function(x, y, speed) {
        this.x = x;
        this.y = y;
        this.speed = speed;
        this.defaultSpeed = speed;
    };

    this.draw = function() {

        if (this.enteringWarp) {
            this.speed = this.speed * this.acceleration;
            if (this.speed > this.warpSpeed) {
                this.speed = this.warpSpeed;
                this.enteringWarp = false;
            }
        } else if (this.leavingWarp) {
            this.speed = this.speed / this.acceleration;
            if (this.speed < this.defaultSpeed) {
                this.speed = this.defaultSpeed;
                this.leavingWarp = false;
            }
        }

        this.y += this.speed;
        this.context.drawImage(this.image, this.x, this.y);
        if (this.repeat) {
            this.context.drawImage(this.image, this.x, this.y - this.canvasHeight);
        }

        if (this.y >= this.canvasHeight && this.repeat) {
            this.y = 0;
        }
    };
}

Background.prototype = new Drawable();